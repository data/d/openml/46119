# OpenML dataset: BitInfoCharts-wo-tweets

https://www.openml.org/d/46119

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bitcoin data scrapped from BitInfoCharts, without the 'tweets' values.

Several Bitcoin related data scrapped directly from BitInfoCharts. 'date' in the format %Y-%m-%d.
The 'tweets' column was dropped due to too many nan values (values only between 2014-04-09 and 2023-03-14).
Besides, we have only keep the rows between the max(dates with non NaN values of each column) and min(dates with non NaN values of each column), which
leave us with dates between 2011-04-14 and 2024-05-26.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46119) of an [OpenML dataset](https://www.openml.org/d/46119). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46119/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46119/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46119/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

